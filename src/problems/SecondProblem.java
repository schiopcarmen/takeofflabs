package problems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class SecondProblem {
    //function finds the numbers that appear more than N/2 times.
    public SecondProblemModel findNumber(ArrayList<Integer> arr) {
        //this will be the returned model
        SecondProblemModel  model = new SecondProblemModel();
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        int arrSize = arr.size();
        int half = Math.round(arrSize / 2);

        for (int i = 0; i < arrSize; i++) {
            if (map.containsKey(arr.get(i))) {
                int count = map.get(arr.get(i)) + 1;
                if (count > half) {
                    //we find a number that appears more than N/2 times
                    model.resultNum.add(arr.get(i));
                    ArrayList<Integer> x =new ArrayList<Integer>();
                    //add it to result
                    x= model.resultNum;
                    return model;
                } else
                    map.put(arr.get(i), count);
            } else
                map.put(arr.get(i), 1);
        }
        return model;
    }
}
