package problems;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class FirstProblem {

    public static void main(String args[]) {
        //first window settings
        SwingUtilities.invokeLater(() -> {
            FirstGUI myFirstGui = new FirstGUI();
            myFirstGui.setLocationRelativeTo(null);
            myFirstGui.setVisible(true);
            myFirstGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        });
    }

    //generateRandomArray function returns an unique random array with the length "l" + 6
    public int[] generateRandomArray(int l) {
        int[] myArray = new int[l];
        //generate an unique list of numbers between -10 and 50
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = -20; i < l+6; i++) {
            list.add(new Integer(i));
        }
        //generate a random array from the above list of numbers
        Collections.shuffle(list);
        for (int j = 0; j < l; j++) {
            myArray[j] = list.get(j);
        }
        return myArray;
    }

    //function returns the number of pairs with the sum equals to "sum" and from "A" array
    public int findPairs(int[] A, int sum) {
        // create an empty Hash Map
        Map<Integer, Integer> map = new HashMap<>();
        int count = 0;
        // do for each element
        for (int i = 0; i < A.length; i++) {
            // check if pair (arr[i], sum-arr[i]) exists
            // if difference is seen before, print the pair and count it
            if (map.containsKey(sum - A[i])) {
                System.out.println("Pair found at index " + map.get(sum - A[i]) + " and " + i);
                count = count + 1;
            }
            // store index of current element in the map
            map.put(A[i], i);
        }
        return count;
    }

    //function return FirstProblemModel (generated array and numbers of pairs)
    //from the input "length" the length of the array and "pairSum" the sum chosen for a pair
    public FirstProblemModel findTheNumbersOfPairs(int length, int pairSum) {
        FirstProblemModel m = new FirstProblemModel();
        //generate a unique random array
        int[] arr = generateRandomArray(length);
        //fin how many pairs of numbers sum up to a given number "pairSum"
        int result = findPairs(arr, pairSum);
        m.generatedArray = arr;
        m.nrPairs = result;
        return m;
    }
}
