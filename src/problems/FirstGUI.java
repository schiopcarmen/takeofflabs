package problems;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirstGUI extends JFrame{
    private JPanel panel1;
    private JTextField textField1;
    private JTextField textField2;
    private JButton computeButton;
    private JTextArea textArea3;
    private JButton nextBtn;
    private JLabel img;
    private JTextArea textArea1;

    public FirstGUI(){
        setTitle("TakeOffLabsJobAppProblems");
        add(panel1);
        setSize(650, 400);
        textArea1.setEditable(false);
        textArea3.setEditable(false);

        FirstProblem f = new FirstProblem();


        computeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //pattern that is ininvalid as input
                Pattern p = Pattern.compile("[a-zA-Z\\-#\\.\\;\\?\\:\\,\\(\\)\\/%&\\s]+", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(textField1.getText());
                boolean b = m.find();
                Matcher n = p.matcher(textField2.getText());
                boolean g = m.find();
                //clear textArea before write
                textArea1.setText("");
                textArea3.setText("");
                //verify if input is valid
                if(textField1.getText().equals("") || textField2.getText().equals("")) {
                    JOptionPane.showMessageDialog(null,"Please give the input!");
                }else if(b || g){
                    JOptionPane.showMessageDialog(null,"Please give a natural number input!");
                }else{
                    //the input from the window
                    FirstProblemModel model = new FirstProblemModel();
                    //output result
                    FirstProblemModel modelResulted = new FirstProblemModel();
                    model.length = Integer.parseInt(textField1.getText());
                    model.sum = Integer.parseInt(textField2.getText());
                    modelResulted = f.findTheNumbersOfPairs(model.length, model.sum);
                    //textArea1 prints the unique random array
                   textArea1.append("[");
                    for (int i = 0; i < model.length; i++) {
                        textArea1.append("  ");
                        textArea1.append(String.valueOf(modelResulted.generatedArray[i]));
                    }
                    textArea1.append("]");
                    //textArea3 prints how many pairs of numbers sum up to a given number S
                    textArea3.append(String.valueOf(modelResulted.nrPairs));
                }
            }
        });
        //open a new window for the second problem
        nextBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SecondGUI sec = new SecondGUI();
                sec.setLocationRelativeTo(null);
                sec.setVisible(true);
            }
        });
    }
    //show the image in window
    private void createUIComponents() {
        img = new JLabel(new ImageIcon("img1.png"));
    }
}
