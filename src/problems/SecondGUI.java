package problems;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SecondGUI extends JFrame {
    private JPanel panel1;
    private JTextArea textArea2;
    private JTextArea textArea3;
    private JPanel backBtn;
    private JTextField textField1;
    private JButton addBtn;
    private JButton computeButton;
    private JButton button1;
    private JLabel imgLabel;
    private JButton clearButton;


    public SecondGUI() {
        SecondProblem s = new SecondProblem();
        setTitle("TakeOffLabsJobAppProblems");
        add(panel1);
        setSize(650, 400);
        textArea2.setEditable(false);
        textArea3.setEditable(false);

        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //pattern that is ininvalid as input
                Pattern p = Pattern.compile("[a-zA-Z\\#\\.\\;\\?\\:\\,\\(\\)\\/%&\\s]+", Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(textField1.getText());
                boolean b = m.find();
                //output result
                SecondProblemModel modelResulted = new SecondProblemModel();
                //verify if input is valid
                if (textField1.getText().equals("") || (b == true)) {
                    JOptionPane.showMessageDialog(null, "Please give a natural number input!");
                } else {
                    textArea2.append(String.valueOf(textField1.getText()));
                    textArea2.append("\n");
                    //clear textField before write
                    textField1.setText("");
                }
            }
        });

        //close the window, go back to first one
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        computeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //the input from the window
                SecondProblemModel model = new SecondProblemModel();
                //output result
                SecondProblemModel modelResulted = new SecondProblemModel();
                String[] lines = textArea2.getText().split("\\n");
                if (lines[0].equals("")) {
                    JOptionPane.showMessageDialog(null, "Please add numbers in array!");
                } else {
                    for (int i = 0; i < lines.length; i++) {
                        model.input.add(Integer.parseInt(lines[i]));
                    }
                    modelResulted = s.findNumber(model.input);
                    //textArea3 prints  the number that appears more than N/2 times
                    if (modelResulted.resultNum.isEmpty()) {
                        textArea3.append("No number!");
                    } else {
                        for (int j = 0; j < modelResulted.resultNum.size(); j++) {
                            textArea3.append(String.valueOf(modelResulted.resultNum.get(j)));
                        }
                    }
                }
            }
        });
        //clear textArea
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea2.setText("");
                textArea3.setText("");
            }
        });
    }
    //show the image in window
    private void createUIComponents() {
        imgLabel = new JLabel(new ImageIcon("img1.png"));
    }
}